﻿using System;

namespace TeamPusheenApp.Common
{
    public class BusinessRuleViolationException : Exception
    {
        public BusinessRuleViolationException(string incorrectTaskStatus) :
            base(incorrectTaskStatus)
        {
        }
    }
}
