﻿using TeamPusheenApp.Data.Entities;
using TeamPusheenApp.Data.QueryProcessors;
using NHibernate;

namespace TeamPusheenApp.Data.SqlServer.QueryProcessors
{
    public class TaskByIdQueryProcessor : ITaskByIdQueryProcessor
    {
        private readonly ISession _session;

        public TaskByIdQueryProcessor(ISession session)
        {
            _session = session;
        }

        public Task GetTask(long taskId)
        {
            var task = _session.Get<Task>(taskId);
            return task;
        }
    }
}
