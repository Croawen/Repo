﻿using TeamPusheenApp.Data.Entities;

namespace TeamPusheenApp.Data.QueryProcessors
{
    public interface IAllTasksQueryProcessor
    {
        QueryResult<Task> GetTasks(PagedDataRequest requestInfo);
    }
}
