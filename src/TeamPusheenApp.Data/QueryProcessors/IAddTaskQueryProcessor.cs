﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamPusheenApp.Data.Entities;

namespace TeamPusheenApp.Data.QueryProcessors
{
    public interface IAddTaskQueryProcessor
    {
        void AddTask(Task task);
    }
}
