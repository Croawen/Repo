﻿using TeamPusheenApp.Data.Entities;

namespace TeamPusheenApp.Data.QueryProcessors
{
    public interface IUpdateTaskStatusQueryProcessor
    {
        void UpdateTaskStatus(Task taskToUpdate, string statusName);
    }
}
