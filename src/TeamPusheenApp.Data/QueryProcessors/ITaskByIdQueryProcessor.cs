﻿using TeamPusheenApp.Data.Entities;

namespace TeamPusheenApp.Data.QueryProcessors
{
    public interface ITaskByIdQueryProcessor
    {
        Task GetTask(long taskId);
    }
}
