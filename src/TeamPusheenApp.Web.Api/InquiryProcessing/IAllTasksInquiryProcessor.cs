﻿using TeamPusheenApp.Data;
using TeamPusheenApp.Web.Api.Models;

namespace TeamPusheenApp.Web.Api.InquiryProcessing
{
    public interface IAllTasksInquiryProcessor
    {
        PagedDataInquiryResponse<Task> GetTasks(PagedDataRequest requestInfo);
    }
}
