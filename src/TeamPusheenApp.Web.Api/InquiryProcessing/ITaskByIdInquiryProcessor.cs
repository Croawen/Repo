﻿using TeamPusheenApp.Web.Api.Models;

namespace TeamPusheenApp.Web.Api.InquiryProcessing
{
    public interface ITaskByIdInquiryProcessor
    {
        Task GetTask(long taskId);
    }
}
