﻿using TeamPusheenApp.Data;
using System;

namespace TeamPusheenApp.Web.Api.InquiryProcessing
{
    public interface IPagedDataRequestFactory
    {
        PagedDataRequest Create(Uri requestUri);
    }
}
