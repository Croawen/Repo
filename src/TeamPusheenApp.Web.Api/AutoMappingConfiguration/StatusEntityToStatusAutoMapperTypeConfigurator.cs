﻿using AutoMapper;
using TeamPusheenApp.Common.TypeMapping;
using TeamPusheenApp.Data.Entities;

namespace TeamPusheenApp.Web.Api.AutoMappingConfiguration
{
    public class StatusEntityToStatusAutoMapperTypeConfigurator : IAutoMapperTypeConfigurator
    {
        public void Configure()
        {
            Mapper.CreateMap<Status, Models.Status>();
        }
    }
}