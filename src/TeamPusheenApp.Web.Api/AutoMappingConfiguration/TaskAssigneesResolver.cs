﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using TeamPusheenApp.Common.TypeMapping;
using TeamPusheenApp.Data.Entities;
using TeamPusheenApp.Web.Common;
using User = TeamPusheenApp.Web.Api.Models.User;

namespace TeamPusheenApp.Web.Api.AutoMappingConfiguration
{
    public class TaskAssigneesResolver : ValueResolver<Task, List<User>>
    {
        public IAutoMapper AutoMapper
        {
            get { return WebContainerManager.Get<IAutoMapper>(); }
        }

        protected override List<User> ResolveCore(Task source)
        {
            return source.Users.Select(x => AutoMapper.Map<User>(x)).ToList();
        }
    }
}