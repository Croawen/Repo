﻿using TeamPusheenApp.Common;
using TeamPusheenApp.Common.TypeMapping;
using TeamPusheenApp.Data.QueryProcessors;
using TeamPusheenApp.Web.Api.Models;
using System.Net.Http;
using TeamPusheenApp.Web.Api.LinkServices;

namespace TeamPusheenApp.Web.Api.MaintenanceProcessing
{
    public class AddTaskMaintenanceProcessor : IAddTaskMaintenanceProcessor
    {
        private readonly IAutoMapper _autoMapper;
        private readonly IAddTaskQueryProcessor _queryProcessor;
        private readonly ITaskLinkService _taskLinkService;

        public AddTaskMaintenanceProcessor(IAddTaskQueryProcessor queryProcessor, IAutoMapper autoMapper, ITaskLinkService taskLinkService)
        {
            _queryProcessor = queryProcessor;
            _autoMapper = autoMapper;
            _taskLinkService = taskLinkService;
        }

        public Task AddTask(NewTask newTask)
        {
            var taskEntity = _autoMapper.Map<Data.Entities.Task>(newTask);
            _queryProcessor.AddTask(taskEntity);
            var task = _autoMapper.Map<Task>(taskEntity);
            _taskLinkService.AddSelfLink(task);

            return task;
        }
    }
}