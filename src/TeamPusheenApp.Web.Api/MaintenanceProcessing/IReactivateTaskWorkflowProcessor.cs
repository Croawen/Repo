﻿using TeamPusheenApp.Web.Api.Models;

namespace TeamPusheenApp.Web.Api.MaintenanceProcessing
{
    public interface IReactivateTaskWorkflowProcessor
    {
        Task ReactivateTask(long taskId);
    }
}
