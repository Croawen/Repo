﻿using System;
using TeamPusheenApp.Web.Api.Models;

namespace TeamPusheenApp.Web.Api.MaintenanceProcessing
{
    public interface ICompleteTaskWorkflowProcessor
    {
        Task CompleteTask(long taskId);
    }
}
