﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamPusheenApp.Web.Api.Models;

namespace TeamPusheenApp.Web.Api.MaintenanceProcessing
{
    public interface IAddTaskMaintenanceProcessor
    {
        Task AddTask(NewTask newTask);
    }
}
