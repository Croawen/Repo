﻿using TeamPusheenApp.Web.Api.Models;

namespace TeamPusheenApp.Web.Api.MaintenanceProcessing
{
    public interface IUpdateTaskMaintenanceProcessor
    {
        Task UpdateTask(long taskId, object taskFragment);
    }
}
