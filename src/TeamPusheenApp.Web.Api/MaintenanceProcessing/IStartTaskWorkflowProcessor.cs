﻿using TeamPusheenApp.Web.Api.Models;

namespace TeamPusheenApp.Web.Api.MaintenanceProcessing
{
    public interface IStartTaskWorkflowProcessor
    {
        Task StartTask(long taskId);
    }
}
