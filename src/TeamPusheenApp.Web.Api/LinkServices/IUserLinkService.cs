﻿using TeamPusheenApp.Web.Api.Models;

namespace TeamPusheenApp.Web.Api.LinkServices
{
    public interface IUserLinkService
    {
        void AddSelfLink(User user);
    }
}
