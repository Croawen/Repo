﻿using TeamPusheenApp.Web.Api.Models;

namespace TeamPusheenApp.Web.Api.LinkServices
{
    public interface ITaskLinkService
    {
        Link GetAllTasksLink();
        void AddSelfLink(Task task);
        void AddLinksToChildObjects(Task task);
    }
}
