﻿using TeamPusheenApp.Common.Security;
using System;

namespace TeamPusheenApp.Web.Common.Security
{
    public interface IWebUserSession : IUserSession
    {
        string ApiVersionInUse { get; }
        Uri RequestUri { get; }
        string HttpRequestMethod { get; }
    }
}
